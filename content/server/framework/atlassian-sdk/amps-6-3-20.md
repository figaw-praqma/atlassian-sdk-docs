---
category: devguide
date: '2018-09-26'
platform: server
product: atlassian-sdk
subcategory: updates
title: AMPS 6.3.20
---
# AMPS 6.3.20

### Release Date: September 26, 2018


### Bug fixes:

- [AMPS-1457](https://ecosystem.atlassian.net/browse/AMPS-1457}) - For JIRA 7.12.2 and later upgrade tomcat8 version to 8.5.32
